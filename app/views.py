from django.shortcuts import render, redirect
from app.models import Pessoa
from app.forms import PessoaForm

#Create
#Read
#Update
#Delete

def list(request):
    pessoa = Pessoa.objects.all()
    return render(request, 'list.html', {'pessoa': pessoa})


def create(request):
    form = PessoaForm(request.POST) # request.POST , é informado no method do html
    if form.is_valid():
        form.save()
        return redirect('list')

    return render(request, 'create.html', {'form': form})

# def update(request):

